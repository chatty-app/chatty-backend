from flask import Blueprint
from flask_restful import Api

rest_api_blueprint = Blueprint('rest_api', __name__)
api = Api(rest_api_blueprint)
