'''
Custom Errors
'''
from os import environ as os_environ


class EntryNotFoundError(Exception):
    '''
    This is the error thrown when an entry was not found in the database.
    May be associated to single-item UPDATE or DELETE operations.
    The id of the entry/item is stored in EntryNotFoundError.entry_id
    '''
    def __init__(self, table, entry_id):
        super(EntryNotFoundError, self).__init__("")
        self.message = "No entry was found in '{}' with id '{}'".format(
            table, entry_id)
        self.table = table
        self.entry_id = entry_id

class UnAuthorizedAccessError(Exception):
    '''
    Thrown when a user tries to perform an unauthorized operation on
    in the database
    '''
    def __init__(self, operation):
        message = "Unauthorized to perform operation : '{}'".format(operation)
        super(UnAuthorizedAccessError, self).__init__(message)
        self.operation = operation

class LessonsClashError(Exception):
    '''
    This is the error thrown when a new lesson being inserted clashes with
    an existing one in the database.
    The lessons with which there was a clash are stored in LessonsClashError.lessons
    '''
    def __init__(self, clashed_lessons):
        super(LessonsClashError, self).__init__(
            "Some lessons clashed")
        self.lessons = clashed_lessons
