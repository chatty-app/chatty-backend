'''
Requests directed towards the 'group_members' endpoint
'''
from flask import jsonify, g, make_response, abort
from flask_restful import Resource

from common.exceptions import  EntryNotFoundError, UnAuthorizedAccessError
from common.status_codes import *
from common.managers.groups_manager import GroupsManager
from common.auth_service import auth_required


class GroupMembersResource(Resource):
    '''
    Handles incoming requests for the 'group_members' endpoint
    '''
    manager = GroupsManager(getattr(g, '_mongo_db'), getattr(g, '_push_service'))
    decorators = [auth_required]

    def get(self, group_id):
        '''
        Get the members of a group
        Params:
            group_id: The _id of the group
                Type: str
        Throws:
            STATUS_NOT_FOUND: If the group was not found in the database
            STATUS_UNAUTHORIZED: If the authenticated user is not a member of the group
        '''
        try:
            group = self.manager.get_single_group(
                group_id, projection=['members', 'members_ids']
            )
        except EntryNotFoundError as err:
            return {'message': err.message}, STATUS_NOT_FOUND
        # if auth_user is not a member of the group, we disallow
        if g.auth_user.id not in group['members_ids']:
            return {
                'message': "Not authorized to preform operation: 'VIEW GROUP MEMBERS'"
            }, STATUS_UNAUTHORIZED
        return make_response(jsonify(group['members']), STATUS_OK)

    def delete(self, group_id, member_id):
        '''
        Delete member from group.
        Params:
            group_id: The group to remove from
                Type: str
            member_id: The _id of the member to remove
                Type: str
        Throws:
            STATUS_NOT_FOUND: If the group or membe was not found in the database
            STATUS_UNAUTHORIZED: If the currently authenticated user is not an admin of the group.
        '''
        try:
            self.manager.delete_group_member(g.auth_user, group_id, member_id)
        except EntryNotFoundError as err:
            return {'message': err.message}, STATUS_NOT_FOUND
        except UnAuthorizedAccessError as unauth_err:
            return {'message': unauth_err.message}, STATUS_UNAUTHORIZED
        return {'message': "Removed successfully"}, STATUS_OK
