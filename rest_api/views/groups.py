'''
Requests directed towards the 'class' endpoint in the api
'''
from flask import jsonify, request, g, make_response, abort
from flask_restful import Resource

from common.exceptions import  EntryNotFoundError, UnAuthorizedAccessError
from common.status_codes import *
from common.managers.groups_manager import GroupsManager
from common.schema import NewGroupSchema, GroupUpdateSchema
from common.auth_service import auth_required


class GroupsResource(Resource):
    '''
    Handles all incoming requests for the 'classes' endpoint
    '''
    manager = GroupsManager(getattr(g, '_mongo_db'), getattr(g, '_push_service'))
    decorators = [auth_required]

    def get(self, group_id=None):
        '''
        Get the data for a single group
        Get the data for multiple groups
        Params:
            group_id: The _id of the group
                Type: 'str'
        '''
        if group_id:
            try:
                data = self.manager.get_single_group(group_id)
            except EntryNotFoundError as err:
                return {'message': err.message}, STATUS_NOT_FOUND
        else:
            data = self.manager.get_multiple_groups()
        response = make_response(jsonify(data), STATUS_OK)
        return response

    def post(self):
        '''
        Create a new group
        '''
        schema = NewGroupSchema()
        data, errors = schema.load(request.form.to_dict())
        if errors:
            return {'errors': errors}, STATUS_INVALID_INPUT
        group_id = self.manager.create_new_group(g.auth_user, data)
        return make_response(jsonify({'id': group_id}), STATUS_CREATED)

    def put(self, group_id):
        '''
        UPDATE the data for a group
        Params:
            group_id: The _id of the group
                Type: 'str'
        '''
        schema = GroupUpdateSchema()
        updates, errors = schema.load(request.form.to_dict())
        if errors:
            return {'errors': errors}, STATUS_INVALID_INPUT
        try:
            self.manager.update_group_info(g.auth_user, group_id, updates)
        except EntryNotFoundError as err:
            return {'message': err.message}, STATUS_NOT_FOUND
        return {'message': 'Updated successfully'}, STATUS_OK

    def delete(self, group_id):
        '''
        DELETE a group
        Params:
            group_id: The _id of the group
                Type: 'str'
        '''
        try:
            self.manager.delete_group_(g.auth_user, group_id)
        except EntryNotFoundError as err:
            return {'message': err.message}, STATUS_NOT_FOUND
        except UnAuthorizedAccessError as unauth_err:
            return {'message': unauth_err.message}, STATUS_UNAUTHORIZED
        message = "The group with id '{}' has been deleted successfully".format(
            group_id)
        return {'message': message}, STATUS_OK
