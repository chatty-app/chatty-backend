'''
Configurations for the flask app
'''
from os import environ as os_environ
import logging


JSON_SORT_KEYS = False

SECRET_KEY = os_environ.get('SECRET_KEY', 'this_should_be_configured')

SMARTFILE_STORAGE_URLS = {
    'images': 'https://file.ac/m9RYGlYL1yw/',
    'audio': 'https://file.ac/qmmHqHY2JBc/',
    'video': 'https://file.ac/_oGYGL6W27I/'
}

FIREBASE_APP_ID = os_environ.get('FIREBASE_APP_ID')
FCM_API_KEY = os_environ.get('FCM_API_KEY')

DEBUG = os_environ.get('FLASK_DEBUG') == '1'

MONGODB_URL = None if DEBUG else os_environ.get('MONGODB_URL')
MONGODB_DB_NAME = 'cHATTY' if DEBUG else os_environ.get('MONGODB_DB_NAME')
