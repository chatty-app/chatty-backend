'''
Operations related to venues
'''
from logging import error as log_error

from bson.errors import InvalidId
from bson.objectid import ObjectId
from pymongo import ASCENDING
from pymongo.errors import PyMongoError

from common.exceptions import EntryNotFoundError


class VenuesManager(object):
    '''
    Contains functions for executing various operations on a venue
    '''

    def __init__(self, mongo_db):
        self.mongo_db = mongo_db

    def create_new_venue(self, venue_data):
        '''
        Create and return a new venue.
        The venue would have been saved in the database.
        '''
        # set default values for fields
        venue_data.update({
            "is_active": True,
            'description': None if 'description' not in venue_data else venue_data['description']
        })
        try:
            venues_collection = self.mongo_db['venues']
            result = venues_collection.insert_one(document=venue_data)
        except PyMongoError as err:
            log_error(
                __name__ + ' >> VenuesManager >> create_new_venue() : ' + err.message
            )
            raise err
        return result.inserted_id

    def get_single_venue(self, venue_id):
        '''
        Returns the data for the venue whose id is as specified in 'venue_id'.
        If no such venue exists, None is returned
        '''
        try:
            venues_collection = self.mongo_db['venues']
            data = venues_collection.find_one(
                filter={'_id': ObjectId(venue_id), 'is_active': True},
                projection=['name', 'description', 'is_shareable']
            )
        except InvalidId:
            raise EntryNotFoundError('venues', venue_id)
        except PyMongoError as err:
            log_error(
                __name__ + " >> venuesManager >> get_single_venue() : " + err.message)
            raise err
        if not data:
            raise EntryNotFoundError('venues', venue_id)
        return data

    def get_multiple_venues(self, query=None):
        '''
        Returns the data for all venues.
        If 'query' is provided, returns only those that matches.
        '''
        query = {} if not query else query
        query.update({'is_active': True})
        try:
            venues_collection = self.mongo_db['venues']
            cur = venues_collection.find(
                filter=query,
                projection=['name', 'description', 'is_shareable'],
                sort=[('name', ASCENDING)]
            )
        except PyMongoError as err:
            log_error(
                'venuesManager >> get_multiple_venues() : ' + err.message)
            raise err
        return [item for item in cur]

    def update_venue_info(self, venue_id, venue_info_updates):
        '''
        Update the info of the venue whose id is as specified in 'venue_id'
        '''
        try:
            venues_collection = self.mongo_db['venues']
            old_info = venues_collection.find_one_and_update(
                filter={'_id': ObjectId(venue_id), 'is_active': True},
                update={'$set': venue_info_updates},
                projection=['name',]
            )
            # # TODO Move this posibly-long-running task to a Cron Service
            # if old_info:  # if a document was actually modified
            #     # if venue's name was changed
            #     if 'name' in venue_info_updates:
            #         # we have to update all nodes in groups where venue's name
            #         # appears
            #         id_obj = ObjectId(venue_id)
            #         new_venue_display_info = {
            #             '_id': id_obj,
            #             'name': venue_info_updates['name']
            #         }
        except InvalidId:
            raise EntryNotFoundError('venues', venue_id)
        except PyMongoError as err:
            log_error(__name__ + ' >> VenuesManager >> update_venue_info() : ' + err.message)
            raise err
        if not old_info:
            raise EntryNotFoundError('venues', venue_id)

    def delete_venue(self, venue_id):
        '''
        Delete the venue whose id is as specified in 'venue_id'
        '''
        venues_collection = self.mongo_db['venues']
        try:
            deleted = venues_collection.find_one_and_update(
                filter={'_id': ObjectId(venue_id)},
                projection=[],
                update={'$set': {'is_active': False}}
            )
        except InvalidId:
            raise EntryNotFoundError('venues', venue_id)
        except PyMongoError as err:
            log_error(
                'venuesManager >> delete_venue() : ' + err.message)
            raise err
        if not deleted:
            raise EntryNotFoundError('venues', venue_id)
