from flask.json import JSONEncoder
from bson import ObjectId

class AppJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            # if the object is an instance if ObjectId
            if isinstance(obj, ObjectId):
                return str(obj)
            # if the object can be iterated, created an iterable from it
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable) # we return the iterable if one was created
        # if it is none of the above
        return JSONEncoder.default(self, obj)
