'''
The urls for the 'rest_api' blueprint
'''

from rest_api.views.users import UsersResource
from rest_api.views.venues import VenuesResource
from rest_api.views.groups import GroupsResource
from rest_api.views.group_lessons import LessonsResource
from rest_api.views.group_members import GroupMembersResource
from rest_api.views.group_join_requests import GroupJoinRequestsResource
from rest_api.views.group_messages import GroupMessagesResource
from rest_api.views.user_groups import UserGroupsResource
from rest_api.views.user_devices import UserDevicesResource
from rest_api.views.i_am import IAmResource
from rest_api import api


def setup_urls():
    '''
    Attach the following urls to the 'flask-restful.Api' object specified in 'api'
    '''
    api.add_resource(
        UsersResource,
        '/users',
        '/users/<user_id>',
        endpoint="users"
    )

    api.add_resource(
        UserGroupsResource,
        '/users/<user_id>/groups',
        '/users/<user_id>/groups/<group_id>',
        endpoint="user_groups"
    )

    api.add_resource(
        UserDevicesResource,
        '/users/<user_id>/devices/<device_id>',
        endpoint="user_devices"
    )

    api.add_resource(
        IAmResource,
        '/i-am',
        endpoint="i_am"
    )

    api.add_resource(
        VenuesResource,
        '/venues',
        '/venues/<venue_id>',
        endpoint='venues'
    )

    api.add_resource(
        GroupsResource,
        '/groups',
        '/groups/<group_id>',
        endpoint="groups"
    )

    api.add_resource(
        LessonsResource,
        '/groups/<group_id>/lessons',
        '/groups/<group_id>/lessons/<lesson_id>',
        endpoint="group_lessons"
    )

    api.add_resource(
        GroupMembersResource,
        '/groups/<group_id>/members',
        '/groups/<group_id>/members/<member_id>',
        endpoint="group_members"
    )

    api.add_resource(
        GroupMessagesResource,
        '/groups/<group_id>/messages',
        endpoint="group_messages"
    )

    api.add_resource(
        GroupJoinRequestsResource,
        '/groups/<group_id>/join-requests',
        '/groups/<group_id>/join-requests/<user_id>/confirm',
        endpoint="group_join_requests"
    )
