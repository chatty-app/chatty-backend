'''
Requests directed towards the 'venue' endpoint in the api
'''
from flask import jsonify, request, g, make_response
from flask_restful import Resource


from common.exceptions import  EntryNotFoundError
                               
from common.status_codes import *
from common.managers.venues_manager import VenuesManager
from common.schema import NewVenueSchema, VenueUpdateSchema
from common.auth_service import auth_required


class VenuesResource(Resource):
    '''
    Handles all incoming requests for the 'venues' endpoint
    '''
    manager = VenuesManager(getattr(g, '_mongo_db'))
    decorators = [auth_required]

    def get(self, venue_id=None):
        '''
        GET the data for the venue whose id is specified.
        GET the data for all venues
        '''
        if venue_id:
            try:
                data = self.manager.get_single_venue(venue_id)
            except EntryNotFoundError as err:
                return {'message': err.message}, STATUS_NOT_FOUND
            response = make_response(jsonify(data), STATUS_OK)
        else:
            data = self.manager.get_multiple_venues()
            response = make_response(jsonify(data), STATUS_OK)
        return response

    def post(self):
        '''
        POST a new venue in the database
        '''
        schema = NewVenueSchema()
        data, errors = schema.load(request.form.to_dict())
        if errors:
            return {'errors': errors}, STATUS_INVALID_INPUT
        venue_id = self.manager.create_new_venue(data)
        return make_response(jsonify({'id': venue_id}), STATUS_CREATED)

    def put(self, venue_id):
        '''
        UPDATE the data for the venue whose id is specified
        '''
        schema = VenueUpdateSchema()
        updates, errors = schema.load(request.form.to_dict())
        if errors:
            return {'errors': errors}, STATUS_INVALID_INPUT
        try:
            self.manager.update_venue_info(venue_id, updates)
        except EntryNotFoundError as err:
            return {'message': err.message}, STATUS_NOT_FOUND
        message = "The venue with id '{}' has been updated successfully"
        return {'message': message}, STATUS_OK

    def delete(self, venue_id):
        '''
        DELETE the venue whose id is specified
        '''
        try:
            self.manager.delete_venue(venue_id)
        except EntryNotFoundError as err:
            return {'message': err.message}, STATUS_NOT_FOUND
        message = "The venue with id '{}' has been deleted successfully".format(
            venue_id)
        return {'message': message}, STATUS_OK
