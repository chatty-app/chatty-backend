from pymongo.mongo_client import MongoClient
from os import environ as os_environ


def init_database(db_name):
    client = MongoClient()
    db = client[os_environ['APP_MONGO_DB_NAME']]

    # create indexes