"""
Flask Documentation:     http://flask.pocoo.org/docs/
Jinja2 Documentation:    http://jinja.pocoo.org/2/documentation/
Werkzeug Documentation:  http://werkzeug.pocoo.org/documentation/
"""

import logging
import os

from flask import Blueprint, Flask, abort, g, request
from flask_restful import Api
# from psycopg2 import connect as connect_db
# from psycopg2 import OperationalError
from pymongo.mongo_client import MongoClient
import urllib, json
from jose import jwt
from jose.exceptions import JWTError    

from common import config
# from common.db_service import DBService as _DBService
from common.json_encoder import AppJSONEncoder
from common.status_codes import (STATUS_INTERNAL_SERVER_ERROR,
                                 STATUS_UNAUTHORIZED)
from common.models import AuthUser
from common.push_service import PushService
from common.auth_service import get_firebase_uid_from_token
from file_uploads import upload_blueprint
from rest_api import rest_api_blueprint


app = Flask(__name__)
app.config.from_object(config)
app.url_map.strict_slashes = False  # allow urls with trailing slashes to be redirected
app.json_encoder = AppJSONEncoder

HOST = app.config['MONGODB_URL']
DB_NAME = str(app.config['MONGODB_DB_NAME'])
logging.error("APP.PY >>>> HOST: " + HOST if HOST is not None else "None")
logging.error("APP.PY >>>> DB_NAME: " + DB_NAME)
MONGO_DB = MongoClient(host=HOST, tz_aware=True)[DB_NAME]
PUSH_SERVICE = PushService(api_key=app.config['FCM_API_KEY'])
# setup the urls for this app
with app.app_context():
    setattr(g, '_mongo_db', MONGO_DB)
    setattr(g, '_push_service', PUSH_SERVICE)
    from rest_api.urls import setup_urls
    setup_urls()

app.register_blueprint(rest_api_blueprint, url_prefix='/api')
app.register_blueprint(upload_blueprint, url_prefix='/file-uploads')


@app.before_request
def authenticate_request():
    '''
    Check if credentials were provided and authenticate accordingly.
    If authentication passes, 'auth_user' attribute of request is set to the authenticated user.
    Else, 'auth_user' atrribute of the request is set to None
    '''
    token = request.headers.get('Authorization')
    if token:
        uid = get_firebase_uid_from_token(token)
        from common.managers.users_manager import UsersManager
        user_manager = UsersManager(MONGO_DB, PUSH_SERVICE)
        users = user_manager.get_multiple_users(
            query={'uid':uid},
            projection=['name', 'email', 'photo_url']
        )
        if len(users) != 1:
            abort(STATUS_UNAUTHORIZED)
        else:
            auth_user = AuthUser(users[0])
            g.auth_user = auth_user
    else:
        g.auth_user = None


# @app.after_request
# def add_header(response):
#     """
#     Add headers to both force latest IE rendering engine or Chrome Frame,
#     and also to cache the rendered page for 10 minutes.
#     """
#     response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
#     response.headers['Cache-Control'] = 'public, max-age=600'
#     return response


# @app.errorhandler(404)
# def page_not_found(error):
#     """Custom 404 page."""
#     return render_template('404.html', **{'error': error}), 404


@app.teardown_appcontext
def teardown_db(exception):
    '''
    Close the database connetion of the current thread when the app is being killed
    '''
    if exception:
        logging.info('teardown_db(exc) >> exc: ' + exception.message)
    logging.error('TEARING DOWN THE APP')

if __name__ == '__main__':
    app.run()
