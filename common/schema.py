from marshmallow import Schema, fields, validate, ValidationError

# TODO Specify an appropriate lower and upper bound of the photo_url fields
class NewUserSchema(Schema):
    '''
    For validating new users' data
    '''
    name = fields.Str(
        required=True, load_only=True,
        validate=[validate.Length(min=5, max=100)]
    )
    email = fields.Str(
        required=True, load_only=True,
        validate=validate.Email(error='Not a valid email address')
    )
    photo_url = fields.Url(
        required=False, load_only=True, allow_none=True,
        validate=validate.Length(min=10)
    )
    firebase_token = fields.Str(required=True)

class UserUpdateSchema(Schema):
    '''
    For validating user updates data
    '''
    name = fields.Str(
        required=False, load_only=True,
        validate=[validate.Length(min=5, max=100)]
    )
    photo_url = fields.Url(
        required=False, load_only=True, allow_none=True,
        validate=validate.Length(min=10)
    )

class NewGroupSchema(Schema):
    '''
    For validating new users data
    '''
    name = fields.Str(
        required=True, load_only=True,
        validate=[validate.Length(min=3, max=100)]
    )
    photo_url = fields.Url(
        required=False, load_only=True, allow_none=True,
        validate=validate.Length(min=10)
    )

class GroupUpdateSchema(Schema):
    '''
    For validatinf groups update data
    '''
    name = fields.Str(
        required=False, load_only=True,
        validate=[validate.Length(min=3, max=100)]
    )
    photo_url = fields.Url(
        required=False, load_only=True, allow_none=True,
        validate=validate.Length(min=10)
    )

class NewVenueSchema(Schema):
    '''
    For validating new venue data
    '''
    name = fields.Str(
        required=True, load_only=True,
        validate=[validate.Length(min=3, max=100)]
    )
    description = fields.Str(
        required=False, load_only=True, allow_none=True,
        validate=[validate.Length(min=3, max=200)]
    )
    is_shareable = fields.Bool(
        required=True, load_only=True
    )

class VenueUpdateSchema(Schema):
    '''
    For validating venue update data
    '''
    name = fields.Str(
        required=False, load_only=True,
        validate=[validate.Length(min=3, max=100)]
    )
    description = fields.Str(
        required=False, load_only=True, allow_none=True,
        validate=[validate.Length(min=3, max=200)]
    )
    is_shareable = fields.Bool(
        required=False, load_only=True
    )


def validate_day_of_week(value):
    '''
    Checks if 'value' is a valid day_of_week
    Params:
        value: The data to check
    Throws:
        ValidationError: if the validation failed
    '''
    valid_days_of_week = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT']
    if value not in valid_days_of_week:
        error_message = "Must be one of [{}]".format(valid_days_of_week)
        raise ValidationError(error_message)

class LessonSchema(Schema):
    '''
    For validating new lesons and lessons updates
    '''
    name = fields.Str(required=True, load_only=True)
    day_of_week = fields.Str(
        required=True, load_only=True, 
        validate=validate_day_of_week
    )
    start_time = fields.DateTime(required=True, load_only=True)
    end_time = fields.DateTime(required=True, load_only=True)
    venue_id = fields.Str(required=True, load_only=True)
    venue_name = fields.Str(required=True, load_only=True)
    venue_description = fields.Str(required=False, load_only=True)

class ClassMessageSchema(Schema):
    id = fields.Int(dump_only=True)
    message = fields.Str(required=True)
    associated_class = fields.Nested('ClassSchema', dump_only=True)
    date_sent = fields.DateTime(required=True)
    expiry_date = fields.DateTime(required=True)
