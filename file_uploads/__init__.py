'''
All file uploads in the application
'''
import imghdr
import logging
import urllib2

from flask import request, Blueprint
from smartfile import BasicClient
from smartfile.errors import APIError

from common.status_codes import *

IMAGES = 'V95YCZpF36E'
AUDIO = 'nYNq4ujvs_A'
VIDEO = '_oGYGL6W27I'
DOCS = 'pl0S0340TUI'
ACCEPTED_FILE_TYPES = ['jpeg', 'png', 'pdf', 'docx', 'doc', 'pptx', 'ppt']

upload_blueprint = Blueprint('uploads', __name__)

@upload_blueprint.route('/file-upload', methods=['POST'])
def upload_file():
    '''
    POST a file. Be it image, audio, video
    '''
    uploaded_file = request.files['file']
    filename = request.form['filename']
    content_type = uploaded_file.content_type
    # remove any dots in the filename and extensions in filename
    if '.' in filename:
        filename = filename[0:filename.index('.')]
    public_url = 'https://file.ac/{folder}/{filename}?download=true'
    smartfile_client = BasicClient()
    if 'image/' in content_type:
        extension = imghdr.what(uploaded_file)
        path = 'path/data/images'
        folder = IMAGES
    elif 'audio/' in content_type:
        extension = content_type[content_type.index('/') + 1:]
        path = 'path/data/audio'
        folder = AUDIO
    elif 'video/' in content_type:
        extension = content_type[content_type.index('/') + 1:]
        path = '/path/data/videos'
        folder = VIDEO
    # elif '/pdf' in content_type or '/doc' in content_type or '/ppt':
    #     extension = content_type[content_type.index('/') + 1:]
    #     path = '/path/data/documents'
    #     folder = DOCS
    else:
        return {'message': 'Invalid file'}, STATUS_INVALID_INPUT
    if extension not in ACCEPTED_FILE_TYPES:
        return {'message': 'Invalid file'}, STATUS_INVALID_INPUT
    filename += '.' + extension
    public_url = public_url.format(
        folder=folder, filename=urllib2.quote(filename))
    try:
        smartfile_client.post(path, file=(filename, uploaded_file))
    except APIError as error:
        logging.error('upload.py: ' + error.message)
        return {'message': 'File upload failed'}, STATUS_FILE_UPLOAD_ERROR
    return {
        'message': 'File upload was successful',
        'public_url': public_url
    }, STATUS_CREATED
