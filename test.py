#!/usr/bin/env python

"""Tests for the Flask Heroku template."""

import unittest
from app import app
from common.db_service import DBService


class TestApp(unittest.TestCase):

    def setUp(self):
        # app.testing = True
        self.app = app.test_client()
        # DBService().db_connection.reset()
        print 'Setting up Here'

    
    def tearDown(self):
        DBService().db_connection.close()
        print "Tearing Down Here"

    def test_home_page_works(self):
        self.assertTrue(True)

    def test_home_page_worked(self):
        self.assertTrue(True)

    # def test_home_page_works(self):
    #     rv = self.app.get('/')
    #     self.assertTrue(rv.data)
    #     self.assertEqual(rv.status_code, 200)

    # def test_about_page_works(self):
    #     rv = self.app.get('/about/')
    #     self.assertTrue(rv.data)
    #     self.assertEqual(rv.status_code, 200)

    # def test_default_redirecting(self):
    #     rv = self.app.get('/about')
    #     self.assertEqual(rv.status_code, 301)

    # def test_404_page(self):
    #     rv = self.app.get('/i-am-not-found/')
    #     self.assertEqual(rv.status_code, 404)

    # def test_static_text_file_request(self):
    #     rv = self.app.get('/robots.txt')
    #     self.assertTrue(rv.data)
    #     self.assertEqual(rv.status_code, 200)
    #     rv.close()


if __name__ == '__main__':
    unittest.main()
