'''
Requests associated to user devices
'''
from flask import request, g, jsonify, make_response
from flask_restful import Resource
from common.exceptions import EntryNotFoundError, UnAuthorizedAccessError
from common.managers.users_manager import UsersManager
from common.auth_service import auth_required
from common.status_codes import STATUS_UNAUTHORIZED, STATUS_NOT_FOUND, STATUS_OK, STATUS_CREATED


class UserDevicesResource(Resource):
    manager = UsersManager(getattr(g, '_mongo_db'),
                           getattr(g, '_push_service'))
    decorators = [auth_required]

    def post(self, user_id, device_id):
        '''
        Add a device to the devices of a user
        Params:
            user_id: The id of the user
            device_id: The id of the device
        '''
        if str(g.auth_user.id) != user_id:
            message = "Not authorized to perform operation: 'ADD USER DEVICE'"
            return make_response(jsonify({'message': message}), STATUS_UNAUTHORIZED)
        try:
            self.manager.add_user_device(user_id, device_id)
        except EntryNotFoundError as err:
            return make_response(jsonify({'message': err.message}), STATUS_NOT_FOUND)
        message = {'message': 'Added successfully'}
        return make_response(jsonify(message), STATUS_CREATED)

    def delete(self, user_id, device_id):
        '''
        Remove a device from the devices of a user
            user_id: The id of the user
            device_id: The id of the device
        '''
        if str(g.auth_user.id) != user_id:
            message = "Not authorized to perform operation: 'ADD USER DEVICE'"
            return make_response(jsonify({'message': message}), STATUS_UNAUTHORIZED)
        try:
            self.manager.delete_user_device(user_id, device_id)
        except EntryNotFoundError as err:
            return make_response(jsonify({'message': err.message}), STATUS_NOT_FOUND)
        message = {'message': 'Removed successfully'}
        return make_response(jsonify(message), STATUS_OK)

    def put(self, user_id, device_id):
        '''
        Replace a user's device
        Params:
            user_id: The id of the user
            device_id: The id of the device to be replaced
        '''
        if str(g.auth_user.id) != user_id:
            message = "Not authorized to perform operation: 'REPLACE USER DEVICE ID'"
            return make_response(jsonify({'message': message}), STATUS_UNAUTHORIZED)
        new_device_id = request.form.to_dict()['new_device']
        try:
            self.manager.update_user_device(user_id, device_id, new_device_id)
        except EntryNotFoundError as err:
            return make_response(jsonify({'message': err.message}), STATUS_NOT_FOUND)
        message = {'message': 'Removed successfully'}
        return make_response(jsonify(message), STATUS_OK)
