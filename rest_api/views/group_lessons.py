'''
Handles all requests associated with the class_lessons endpoint
'''
from flask_restful import Resource
from flask import request, jsonify, g, abort, make_response
from common.exceptions import EntryNotFoundError, UnAuthorizedAccessError
from common.status_codes import STATUS_UNAUTHORIZED, STATUS_NOT_FOUND, STATUS_OK, STATUS_INVALID_INPUT
from common.managers.groups_manager import GroupsManager
from common.schema import LessonSchema
from common.auth_service import auth_required

class LessonsResource(Resource):
    '''
    Handles the creation, listing and editting of lessons.
    endpoint: lessons
    '''
    manager = GroupsManager(getattr(g, '_mongo_db'), getattr(g, '_push_service'))
    decorators = [auth_required]

    def get(self, group_id):
        '''
        Get the lessons for a group
        Params:
            group_id: The _id of the group
                Type: 'str'
        '''
        try:
            lessons = self.manager.get_group_lessons(g.auth_user, group_id)
        except EntryNotFoundError as not_found_err:
            return {'message':not_found_err.message}, STATUS_NOT_FOUND
        except UnAuthorizedAccessError as unauth_err:
            return {'message': unauth_err.message}
        return make_response(jsonify(lessons), STATUS_OK)

    def post(self, group_id):
        '''
        Add new lesson to a group
        Params:
            group_id: The _id of the group
                Type: 'str'
        Throws:
            EntryNotFoundError: If the group was not found
            UnAuthorizedAccessError: If the current authed user is not an admin of the group
        '''        
        schema = LessonSchema()
        data, errors = schema.load(request.form.to_dict())
        if errors:
            return {'errors': errors}, STATUS_INVALID_INPUT
        try:
            if 'description' not in data:
                data.update({'description': None})
            lesson_id = self.manager.add_group_lesson(
                initiator=g.auth_user, group_id=group_id, **data
            )
        except EntryNotFoundError as not_found_err:
            return {'message': not_found_err.message}, STATUS_NOT_FOUND
        except UnAuthorizedAccessError as unauth_err:
            return {'message': unauth_err.message}, STATUS_UNAUTHORIZED
        return make_response(jsonify({'id':lesson_id}), STATUS_OK)

    # def put(self, group_id, lesson_id):
    #     '''
    #     For the group with the specified group_id,
    #     UPDATE the lesson with the specified lesson_id
    #     '''
    #     data = request.form.to_dict()
    #     if len(data) == 0:
    #         return {
    #             'message': 'No input data provided'
    #         }, STATUS_NO_INPUT
    #     data.update({'group_id': group_id, 'lesson_id': lesson_id})
    #     try:
    #         self.db_service.update_group_lesson_data(**data)
    #     except EntryNotFoundError:
    #         message = 'No lesson with id {} was found for the group with id {}'.\
    #             format(lesson_id, group_id)
    #         return {
    #             'message': message
    #         }, STATUS_NOT_FOUND
    #     except InvalidColumnsError as invalid_col_error:
    #         invalid_col_str = ', '.join(invalid_col_error.columns)
    #         return {
    #             'message': 'Unexpected data input(s): [' + invalid_col_str + ']'
    #         }, STATUS_INVALID_INPUT
    #     except DBError as db_error:
    #         return {
    #             'message': db_error.message,
    #             'error_code': db_error.error_code
    #         }, STATUS_DB_ERROR

    def delete(self, group_id, lesson_id):
        '''
        For the group with the specified group_id,
        DELETE the lesson with the specified lesson_id
        '''
        try:
            self.manager.delete_group_lesson(g.auth_user, group_id, lesson_id)
        except EntryNotFoundError as not_found_err:
            return {'message': not_found_err.message}, STATUS_NOT_FOUND
        except UnAuthorizedAccessError as unauth_err:
            return {'message': unauth_err.message}, STATUS_UNAUTHORIZED
        return {'message':'Deleted successfully'}, STATUS_OK
