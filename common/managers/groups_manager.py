'''
Operations related to groups
'''
from datetime import datetime
from logging import error as log_error

# import pytz
from bson import ObjectId
from bson.errors import InvalidId
from pymongo import ASCENDING
from pymongo.errors import PyMongoError

from common.exceptions import EntryNotFoundError, UnAuthorizedAccessError, LessonsClashError
from common.push_service import PushNotificationBuilder, GroupMessageBuilder


class GroupsManager(object):
    '''
    Contains functions for executing various operations on a group
    '''

    def __init__(self, mongo_db, push_service):
        self.mongo_db = mongo_db
        self.push_service = push_service

    def create_new_group(self, creator, group_data):
        '''
        Create and return a group.
        The group would have been saved in the database.
        'Creator must be an instance of User.
        '''
        current_datetime = datetime.utcnow()
        from common.managers.users_manager import UsersManager
        creator_devices = UsersManager(self.mongo_db, self.push_service).get_single_user(
            creator.id, projection=['devices']
        )['devices']
        creator_info = {'_id': creator.id,
                        'name': creator.name, 'photo_url': creator.photo_url}
        group_data.update({
            'creator': creator_info,
            'admins_ids': [creator.id], 'admins': [creator_info],
            'members_ids': [creator.id], 'members': [creator_info],
            'date_created': current_datetime, 'is_active': True,
            'members_devices': creator_devices, 'join_requests': [], 'lessons': [],
            'photo_url': None if 'photo_url' not in group_data else group_data['photo_url'],
            'messages': [{
                '_id': ObjectId(), 'message': '{} created group'.format(creator.name),
                'media': None, 'sender': None, 'prioritized': False,
                'timestamp': current_datetime, 'timezone_offset': '+00:00'
            }]
        })
        try:
            groups_collection = self.mongo_db['groups']
            result = groups_collection.insert_one(document=group_data)
        except PyMongoError as err:
            log_error('GroupsManager >> new_group_() : ' + err.message)
            raise err
        return result.inserted_id

    def get_single_group(self, group_id, projection=None):
        '''
        Returns the data for a single group.
        Params:
            group_id: The _id of the group to select
                Type: str
            projection: List of the data fields to return. The data fiields are strings.
                Type: list
        '''
        if projection is None:
            fields_to_select = [
                'name', 'creator', 'photo_url', 'date_created',
                'admins', 'members', 'lessons'
            ]
        else:
            fields_to_select = projection
        try:
            groups_collection = self.mongo_db['groups']
            data = groups_collection.find_one(
                filter={'_id': ObjectId(group_id), 'is_active': True},
                projection=fields_to_select
            )
        except InvalidId:
            raise EntryNotFoundError('groups', group_id)
        except PyMongoError as err:
            log_error(
                "GroupsManager >> get_single_group_() : " + err.message)
            raise err
        if not data:
            raise EntryNotFoundError('groups', group_id)
        return data

    def get_multiple_groups(self, query=None, projection=None):
        '''
        Returns the data for all groups.
        If 'query' is provided, returns only those that matches.
        '''
        query = {} if not query else query
        query.update({'is_active': True})
        if projection is None:
            fields_to_select = ['name', 'creator', 'photo_url', 'date_created']
        else:
            fields_to_select = projection
        try:
            groups_collection = self.mongo_db['groups']
            cur = groups_collection.find(
                filter=query,
                # fields which will be returned from the database
                projection=fields_to_select,
                sort=[('name', ASCENDING)]
            )
        except PyMongoError as err:
            log_error(
                'GroupsManager >> get_multiple_group_s() : ' + err.message)
            raise err
        data = []
        for item in cur:
            data.append(item)
        return data

    def update_group_info(self, initiator, group_id, updates):
        '''
        Update the data (name and/or photo_url) for a group
        Params:
            initiator: The currently authenticated user performing the operation
                Type: 'common.models.AuthUser'
            group_id: The _id of the group
                Type: 'str'
            updates: Key-value pairs, which contains the updates of the group
                Type: 'dict'
        '''
        group = self.get_single_group(group_id, projection=['admins_ids'])
        initiator_is_authorized = initiator.id in group['admins_ids']
        if not initiator_is_authorized:
            raise UnAuthorizedAccessError('UPDATE GROUP INFO')
        try:
            groups_collection = self.mongo_db['groups']
            groups_collection.find_one_and_update(
                filter={'_id': group['_id'], 'is_active': True},
                update={'$set': updates}, projection=[]
            )
        except PyMongoError as err:
            log_error('GroupsManager >> update_group_info() : ' + err.message)
            raise err
        notification = PushNotificationBuilder.build_group_info_update_notification(group_id)
        self.push_service.notify_devices(data=notification, device_ids=group['members_devices'])

    def delete_group_(self, initiator, group_id):
        '''
        Delete a group
        Params:
            initiator: The currently authenticated user performing this operation
                Type: 'common.models.AuthUser'
            group_id: The _id of the group
                Type: 'str'
        Throws:
            EntryNotFoundError: If the group is not found
            UnAuthorizedAccessError: If the operation is not authorized. This happens if:
                                    1.  The initiator is not the creator of the group
        '''
        group = self.get_single_group(group_id, projection=['creator'])
        initiator_is_authorized = initiator.id = group['creator']['_id']
        if not initiator_is_authorized:
            raise UnAuthorizedAccessError('DELETE GROUP')
        try:
            self.mongo_db['groups'].find_one_and_update(
                filter={'_id': group['_id'], 'is_active': True},
                projection=[],
                update={'$set': {'is_active': False}}
            )
        except PyMongoError as err:
            log_error(
                'GroupsManager >> delete_group_() : ' + err.message)
            raise err
        # TODO Notify the group's members that the group has been deleted

    def get_requests_to_join_group(self, initiator, group_id):
        '''
        Returns the data of all users who have requested to join a group
        Params:
            initiator: The user who is initiating this operation
                Type: 'common.models.AuthUser'
            group_id: The _id of the group
                Type: 'str'
        Throws:
            EntryNotFoundError: If no group with such id exists
            UnAuthorizedAccessError: If the initiator is not authorized
        '''
        try:
            group = self.get_single_group(
                group_id,
                projection=['admins_ids', 'join_requests']
            )
            # check if the iniator is authorized to access this info
            initiator_is_authorized = initiator.id in group['admins_ids']
            if not initiator_is_authorized:
                raise UnAuthorizedAccessError('VIEW GROUP JOIN REQUESTS')
            from common.managers.users_manager import UsersManager
            usermanager = UsersManager(self.mongo_db, self.push_service)
            requesting_users = usermanager.get_multiple_users(
                query={
                    '_id': {'$in': group['join_requests']},
                    'is_active': True
                },
                projection=['name', 'email', 'photo_url', '_id']
            )
        except PyMongoError as err:
            log_error(
                __name__ +
                ' >> GroupsManager >> get_group_member_join_requests() : ' +
                err.message
            )
            raise err
        return requesting_users

    def make_request_to_join_group(self, group_id, user):
        '''
        Add user to the list of join_requests of a group
        Params:
            group_id: The _id of the group
                Type: 'str'
            user: The user to make the request for
                Type: 'common.models.AuthUser'
        Throws:
            EntryNotFoundError: If the group was not found
        '''
        group = self.get_single_group(
            group_id,
            projection=['join_requests', 'members_ids']
        )
        # if the user is already a member of the group, we disallow
        if user.id in group['members_ids']:
            raise UnAuthorizedAccessError('REQUEST TO JOIN GROUP')
        try:
            self.mongo_db['groups'].find_one_and_update(
                filter={'_id': group['_id']},
                update={'$addToSet': {'join_requests': user.id}},
                projection=[]
            )
        except PyMongoError as err:
            log_error(
                __name__ + ' >> GroupsManager >> make_request_to_join_group() : ' + err.message)

    def cancel_request_to_join_group(self, initiator, group_id, user_id):
        '''
        Cancel a user's request to join a group.
        The user is removed from the group's join_requests
        Params:
            initiator: The currently authed user performing this action.
                Type: 'common.models.AuthUser'
            group_id: The _id of the group.
                Type: 'str'
            user: The user to cancel the request for.
                Type: 'common.models.AuthUser'
        Throws:
            EntryNotFoundError: If the group or user was not found.
            UnAuthorizedAccessError: If the operation is not authorized. This happens if:
                                    1.  The initiator is neither an admin of the group nor
                                        the user whose request is being cancelled.
        '''
        group = self.get_single_group(
            group_id,
            projection=['admins_ids']
        )
        groups_collection = self.mongo_db['groups']
        initiator_is_authorized = str(
            initiator.id) == user_id or initiator.id in group['admins_ids']
        if not initiator_is_authorized:
            raise UnAuthorizedAccessError('CANCEL GROUP JOIN REQUEST')
        try:
            groups_collection.find_one_and_update(
                filter={'_id': group['_id']},
                update={'$pull': {'join_requests': ObjectId(user_id)}}
            )
        except InvalidId:
            raise EntryNotFoundError('users', user_id)
        except PyMongoError as err:
            log_error(
                __name__ + " >> GroupsManager >> cancel_request_to_join_group() : " + err.message)
            raise err

    def confirm_request_to_join_group(self, initiator, group_id, user_id):
        '''
        Confirm a user's request to join a group.
        Adds the user to the list of members of thr group.
        Params:
            initiator: The auth_user performing the operation.
                Type: 'common.models.AuthUser'
            group_id: The _id of the group.
                Type: 'str'
            user_id: The _id of the user for whom the confirmation is being made
                Type: 'str'
        Throws:
            EntryNotFoundError: If the group or the user is not found
            UnAuthorizedAccessError: If the action is not authorized. This happens if:
                                    1.  The initiator is not an admin of the group
                                    2.  The user has not made a request to join group
        '''
        group = self.get_single_group(
            group_id,
            projection=['join_requests', 'admins_ids']
        )
        from common.managers.users_manager import UsersManager
        user = UsersManager(self.mongo_db, self.push_service).get_single_user(
            user_id,
            projection=['name', 'photo_url', 'devices']
        )
        # check if the initiator is an admin of the group
        if initiator.id not in group['admins_ids']:
            raise UnAuthorizedAccessError('CONFIRM MEMBER JOIN REQUEST')
        if user['_id'] not in group['join_requests']:
            raise UnAuthorizedAccessError(
                'ADD GROUP MEMBER WITHOUT PRIOR REQUEST')
        try:
            # add user to group's members and remove from join_requests
            self.mongo_db['groups'].find_one_and_update(
                filter={'_id': group['_id']},
                update={
                    '$push': {
                        'members': {
                            '_id': user['_id'], 'name': user['name'], 'photo_url': user['photo_url']
                        },
                    },
                    '$addToSet': {
                        'members_ids': user['_id'],
                        'members_devices': {'$each': user['devices']}
                    },
                    '$pull': {
                        'join_requests': user['_id']
                    }
                },
                projection=[]
            )
            # add the group to the user's list of groups
            self.mongo_db['users'].find_one_and_update(
                filter={'_id': user['_id']},
                update={
                    '$push': {'groups': group['_id']}
                },
                projection=[]
            )
        except PyMongoError as err:
            log_error(
                __name__ + ' >> GroupsManager >> confirm_request_to_join_group() : ' + err.message)
            raise err

    def delete_group_member(self, initiator, group_id, member_id):
        '''
        Remove member from group.
        Params:
            initiator: The user who initiated this action.
                Type: 'common.models.AuthUser'
            group_id: The _id of the group to remove from.
                Type: 'str'
            member_id: The _id of the member to be removed.
                Type: 'str'
        Throws:
            UnAuthorizedAccessError: If the initiator is not allowed to perform the operation.
            EntryNotFoundError: If the group or member was not found in the database
        '''
        is_self = str(initiator.id) == member_id
        group = self.get_single_group(
            group_id,
            projection=['creator', 'members_ids' if is_self else 'admins_ids']
        )
        if is_self:
            # the creator of a group cannot leave the group
            if group['creator']['_id'] == initiator.id:
                raise UnAuthorizedAccessError('LEAVE GROUP')
            # check if the initiator is a member of the group
            if initiator.id not in group['members_ids']:
                raise UnAuthorizedAccessError('DELETE MEMBER FROM GROUP')
        else:
            # disallow if the initiator is deleting the creator of the group
            if group['creator']['_id'] == ObjectId(member_id):
                raise UnAuthorizedAccessError('DELETE GROUP CREATOR')
                # check if the initiator is an admin of the group
            if not initiator.id in group['admins_ids']:
                raise UnAuthorizedAccessError('DELETE MEMBER FROM GROUP')
        from common.managers.users_manager import UsersManager
        usermanager = UsersManager(self.mongo_db, self.push_service)
        # the member to be removed
        member = usermanager.get_single_user(
            user_id=member_id, projection=['name', 'devices']
        )
        try:
            self.mongo_db['groups'].find_one_and_update(
                filter={'_id': group['_id']},
                update={
                    # remove from members_ids, members and members_devices
                    '$pull': {
                        'members_ids': member['_id'],
                        'members': {'_id': member['_id']},
                        'members_devices': member['devices']
                    }
                },
                projection=['name']
            )
            # remove the group from the list of the user's groups
            self.mongo_db['users'].find_one_and_update(
                filter={'_id': member['_id']},
                update={'$pull': {'groups': group_id}}
            )
        except PyMongoError as err:
            log_error(
                __name__ + ' >> GroupsManager >> delete_group_member() : ' + err.message)
            raise err
        # if the initiator is removing himself
        if is_self:
            grp_msg_txt = initiator.name + ' left'
        else:
            user_msg_txt = initiator.name + \
                " removed you from group '" + group['name'] + "'"
            # notify member that he/she has been removed from group
            self.push_service.notify_devices(
                message={'message': user_msg_txt},
                device_ids=member['devices']
            )
            grp_msg_txt = initiator.name + " removed " + member['name']
        # notify the group that member is no more part of group
        self.send_message_to_group(
            group_id,
            message={
                "_id": ObjectId(), "message": grp_msg_txt, "media": None,
                "sender": None, "prioritized": False, "time": datetime.utcnow()
            }
        )

    def get_group_lessons(self, initiator, group_id):
        '''
        Returns the lessons of a group
        Params:
            initiator: The current authed user who is performing the operation
                Type: 'common.models.AuthUser'
            group_id: The _id of the group
                Type: 'str'
        Throws:
            EntryNotFoundError: If the group is not found
            UnAuthorizedAccessError: If the initiator is not a member of the group
        '''
        group = self.get_single_group(
            group_id,
            projection=['members_ids', 'lessons']
        )
        # only members are allowed to access group lessons
        initiator_is_authorized = initiator.id in group['members_ids']
        if not initiator_is_authorized:
            raise UnAuthorizedAccessError('VIEW GROUP LESSONS')
        return group['lessons']

    def add_group_lesson(
            self, initiator, group_id, name, day_of_week,
            start_time, end_time, venue_id):
        '''
        Add a new lesson to a group.
        Params:
            initiator: The current authed user who is performing the operation
                Type: 'common.models.AuthUser'
            group_id: The _id of the group
                Type: 'str'
        Return: The _id of the added lesson
            Type: 'bson.objectid.ObjectId'
        Throws:
            EntryNotFoundError: If the group or venue is not found
            UnAuthorizedAccessError: If the initiator is not an admin of the group
            LessonsClashError: If any of the existing lessons of the group clashes
                                with this new lesson
        '''
        group = self.get_single_group(
            group_id, projection=['admins_ids', 'lessons']
        )
        initiator_is_authorized = initiator.id in group['admins_ids']
        if not initiator_is_authorized:
            raise UnAuthorizedAccessError('ADD GROUP LESSON')
        try:
            venue = self.mongo_db['venues'].find_one(
                filter={'_id': ObjectId(venue_id)})
        except InvalidId:
            raise EntryNotFoundError('venues', venue_id)
        clashed_lessons = []
        for lesson in group['lessons']:
            is_on_same_day_of_week = lesson['day_of_week'] == day_of_week
            starts_before = lesson['start_time'] <= start_time
            ends_before = lesson['end_time'] <= start_time
            starts_after = lesson['start_time'] >= end_time
            ends_after = lesson['end_time'] >= end_time
            # the time position of the new lesson
            happens_before = starts_before and ends_before
            happens_after = starts_after and ends_after
            happens_before_or_after = happens_before or happens_after
            if is_on_same_day_of_week and not happens_before_or_after:
                clashed_lessons.append(lesson)
        if not len(clashed_lessons) == 0:
            raise LessonsClashError(clashed_lessons)
        lesson_to_insert = {
            'name': name, 'start_time': start_time, 'end_time': end_time,
            'day_of_week': day_of_week, 'venue': {
                '_id': venue['_id'], 'name': venue['name'],
                'description': venue['description']
            }, '_id': ObjectId()
        }
        try:
            self.mongo_db['groups'].find_one_update(
                filter={'_id': group['_id']},
                update={'$push': {'lessons': lesson_to_insert}}
            )
        except PyMongoError as err:
            log_error(
                __name__ + ' >> GroupsManager >> add_group_lessons() : ' + err.message)
            raise err

    def update_group_lesson(self):
        pass

    def delete_group_lesson(self, initiator, group_id, lesson_id):
        '''
        Delete a lesson of a group
        Params:
            group_id: The _id of the group
                Type: 'str'
            lesson_id: The _id of the lesson
                Type: 'str'
        Throws:
            EntryNotFoundError: If the group or lesson is not found
            UnAuthorizedAccessError: If the initiator is not an admin of the group
        '''
        group = self.get_single_group(
            group_id, projection=['admins_ids']
        )
        initiator_is_authorized = initiator.id in group['admins_ids']
        if not initiator_is_authorized:
            raise UnAuthorizedAccessError('DELETE GROUP LESSON')
        try:
            updated = self.mongo_db['groups'].find_one_and_update(
                filter={'_id': group['_id']},
                update={
                    '$pull': {'lessons': {'id': ObjectId(lesson_id)}}
                }
            )
        except InvalidId:
            raise EntryNotFoundError('groups lessons', lesson_id)
        except PyMongoError as err:
            log_error(
                __name__ + ' >> GroupsManager >> delete_group_messages() : ' + err.message)
            raise err
        if not updated:
            raise EntryNotFoundError('groups', group_id)

    def notify_group_admins(self, group_id, message):
        '''
        Send a notificatioin/message to all the admins of a group.
        The notification/message not stored in the group_document in groups_collection
        Params:
            group_id: The _id of the group
                Type: 'str'
            message: The message to send
                Type: 'text'
        Throws:
            EntryNotFoundError: If the group was not found
        '''
        group = self.get_single_group(group_id, projection=['admins_ids'])
        from common.managers.users_manager import UsersManager
        admins = UsersManager(self.mongo_db, self.push_service).get_multiple_users(
            query={'_id': {'$in': group['admins_ids']}},
            projection=['devices']
        )
        recipient_devices = []
        for admin in admins:
            recipient_devices += admin['devices']
        message_to_send = {
            'message': message, 'group_id': group_id,
            'time': datetime.utcnow().isoformat(),
            'type': 'NOTIFICATION'
        }
        self.push_service.notify_devices(message_to_send, recipient_devices)

    def send_message_to_group(self, group_id, message=None, media=None, sender=None, prioritized=False, time_to_use=None):
        '''
        Send message into group.
        All members of the group will receive this message eventually.
        The message is stored in the group_document in groups_collection
        Params:
            group_id: 'The _id of group to send the message to'
                Type: str
            message: 'The message to send to the group'
                Type: 'str'
        Throws:
            ValueError: If both the message and media fields are None
        '''
        # at least one of 'message' and 'media' must not be null
        if message.get('message') is None and message.get('media') is None:
            raise ValueError("One of 'message' and 'media' must be non-null")
        time_sent = datetime.utcnow() if time_to_use is None else time_to_use
        message_id = ObjectId()
        group_id = ObjectId(group_id)
        message_to_store = {
            '_id': message_id, 'message': message, 'media': media,
            'time': time_sent, 'sender': sender, 'prioritized': prioritized
        }
        if sender is None:
            sender_str = ''
        else:
            sender_str = {
                '_id': str(sender.id), 'name': sender.name, 'photo_url': sender.photo_url
            }
        message_to_send = {
            'group_id': str(group_id), 'message_id': str(message_id),
            'message': message, 'media': media, 'time': time_sent.isoformat(),
            'sender': sender_str, 'prioritized': prioritized,
        }
        try:
            group = self.mongo_db['groups'].find_one_and_update(
                filter={'_id': group_id},
                update={'$push': {'messages': message_to_store}},
                projection=['members_devices']
            )
        except PyMongoError as err:
            raise err
        if group is None:
            raise EntryNotFoundError('groups', group_id)
        # send the message via FCM
        self.push_service.notify_devices(
            message=message_to_send, devices=group['members_devices']
        )
