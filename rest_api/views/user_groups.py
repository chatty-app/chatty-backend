'''
User groups related requests
'''

from flask import g, make_response, jsonify, abort
from flask_restful import Resource
from common.exceptions import EntryNotFoundError, UnAuthorizedAccessError
from common.managers.users_manager import UsersManager
from common.auth_service import auth_required
from common.status_codes import STATUS_UNAUTHORIZED, STATUS_NOT_FOUND, STATUS_OK


class UserGroupsResource(Resource):
    '''
    Users groups access
    '''
    manager = UsersManager(getattr(g, '_mongo_db'), getattr(g, '_push_service'))
    decorators = [auth_required]

    def get(self, user_id):
        '''
        GET the data for the groups of a user
        Params:
            user_id: The _id of the user
                Type: 'str'
        '''
        if str(g.auth_user.id) != user_id:
            message = "Not authorized to perform operation: 'GET USER GROUPS'"
            return {'message': message}, STATUS_UNAUTHORIZED
        try:
            groups = self.manager.get_user_groups(user_id)
        except EntryNotFoundError as err:
            return {'message': err.message}, STATUS_NOT_FOUND
        return make_response(jsonify(groups), STATUS_OK)

    def delete(self, user_id, group_id):
        '''
        Delete/Remove a user from his/her group
        Params:
            user_id: The _id of the user. Must be the same as the _id
                        of the current authed user
                Type: 'str'
            group_id: The _id of the group
                Type: 'str'
        '''
        if str(g.auth_user) != user_id:
            message = "Not authorized to perform operation: 'REMOVE USER FROM GROUP'"
            return {'message': message}, STATUS_UNAUTHORIZED
        try:
            self.manager.leave_group(g.auth_user, group_id)
        except EntryNotFoundError as not_found_err:
            return {'message': not_found_err.message}, STATUS_NOT_FOUND
        except UnAuthorizedAccessError as unauth_err:
            return {'message': unauth_err.message}, STATUS_UNAUTHORIZED
