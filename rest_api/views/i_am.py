'''Requests associate to retrieving a user's own information
'''
from flask import request, g, make_response, jsonify, abort
from flask_restful import Resource

from common.status_codes import STATUS_UNAUTHORIZED, STATUS_NOT_FOUND, STATUS_OK
from common.managers.users_manager import UsersManager
from common.exceptions import EntryNotFoundError


class IAmResource(Resource):
    '''
    Get the info of a user, his profile, his groups.
    '''
    manager = UsersManager(getattr(g, '_mongo_db'), getattr(g, '_push_service'))
    def get(self):
        '''
        Get the information of the currently logged in user
        '''
        if g.auth_user is None:
            abort(STATUS_UNAUTHORIZED)
        try:
            user = self.manager.get_single_user(str(g.auth_user.id))
            groups = self.manager.get_user_groups(str(g.auth_user.id))
        except EntryNotFoundError as err:
            return {'message': err.message}, STATUS_NOT_FOUND
        data = {
            'user': user,
            'groups': groups
        }
        response = make_response(jsonify(data), STATUS_OK)
        return response
