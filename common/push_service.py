# pylint: disable-msg=C0103
'''
Push notifications related operations
'''
from os import environ as os_environ
from pyfcm import FCMNotification

class PushNotificationBuilder(object):
    '''
    Construct the payload for the push notification types of the app
    '''
    NOTIFICATION = 'NOTIFICATION'
    # Types of NOTIFICATION
    GROUP_INFO_UPDATE_NOTIFICATION = 'GROUP_INFO_UPDATE_NOTIFICATION'
    GROUP_DELETED_NOTIFICATION = 'GROUP_DELETED_NOTIFICATION'
    GROUP_TIMETABLE_UPDATE_NOTIFICATION = 'GROUP_TIMETABLE_UPDATE_NOTIFICATION'
    GROUP_MEMBER_DELETION_NOTIFICATION = 'GROUP_MEMBER_DELETION_NOTIFICATION'
    GROUP_JOIN_REQUEST_CONFIRMED_NOTIFICATION = 'GROUP_JOIN_REQUEST_UPDATED_NOTIFICATION'

    @staticmethod
    def build_group_join_request_confirmed_notification(group_id, group_name):
        '''
        Construct the notification data message for confirmation of a user's request to join a group
        Params:
            group_id: The _id of the group
                Type: 'str'
            group_name: The name of the group
                Type: 'str'
        '''
        data_message = {
            'category': PushNotificationBuilder.NOTIFICATION,
            'type': PushNotificationBuilder.GROUP_JOIN_REQUEST_CONFIRMED_NOTIFICATION,
            'group_id': group_id, 'group_name': group_name
        }
        return data_message

    @staticmethod
    def build_group_member_deletion_notification(group_id, admin_name):
        '''
        Construct the notification data message for a group member deletion
        Params:
            group_id: The _id of the group from which the member was deleted
                Type: 'str'
            admin_name: The name of the admin who deleted the member
                TypeL 'str'
        '''
        data_message = {
            'category': PushNotificationBuilder.NOTIFICATION,
            'type': PushNotificationBuilder.GROUP_MEMBER_DELETION_NOTIFICATION,
            'group_id': group_id, 'admin_name': admin_name
        }
        return data_message

    @staticmethod
    def build_group_timetable_updated_notification(group_id):
        '''
        Construct the notification data message for updates of group lessons
        Params:
            group_id: The _id of the group whose timetable has been updated
                Type: 'str'
        '''
        data_message = {
            'category': PushNotificationBuilder.NOTIFICATION,
            'type': PushNotificationBuilder.GROUP_TIMETABLE_UPDATE_NOTIFICATION,
            'group_id': group_id
        }
        return data_message

    @staticmethod
    def build_group_deleted_notification(group_id):
        '''
        Construct the notification data message for the deletion of a group
        Params:
            group_id: The _id of the deleted group
                Type: 'str'
        '''
        data_message = {
            'category': PushNotificationBuilder.NOTIFICATION,
            'type': PushNotificationBuilder.GROUP_DELETED_NOTIFICATION,
            'group_id': group_id
        }
        return data_message

    @staticmethod
    def build_group_info_update_notification(group_id, new_name, new_photo_url):
        '''
        Construct the notification data message for group info updates
        Params:
            group_id: The _id of the updated group
                Type: 'str'
            new_name: The new name of the group
                Type: 'str'
            new_photo_url: The new photo_url of the group
                Type: 'str'                
        '''
        data_message = {
            'category': PushNotificationBuilder.NOTIFICATION,
            'type': PushNotificationBuilder.GROUP_INFO_UPDATE_NOTIFICATION,
            'group_id': group_id, 'new_name': new_name,
            'new_photo_url': new_photo_url
        }
        return data_message


class GroupMessageBuilder(object):
    '''
    Construct the payload for the various message types of a group
    '''
    MESSAGE = 'MESSAGE'

    # Types of group messages
    PRIORITIZED_MESSAGE = 'PRIORITIZED_MESSAGE'
    CHAT_MESSAGE = 'CHAT_MESSAGE'
    SYSTEM_MESSAGE = 'SYSTEM_MESSAGE'

    @staticmethod
    def build_prioritized_message(group_id, sender_id, message, time):
        '''
        Construct prioritized message for a group
        Params:
            group_id: The _id of the group
                Type: 'str'
            sender_id: The _id of the message sender
                Type: 'str'
            message: The message to be delivered
                Type: 'str'
            time: The time the message was sent
                Type: 'datetime.datetime'
        '''
        data_message = {
            'category': GroupMessageBuilder.MESSAGE,
            'type': GroupMessageBuilder.PRIORITIZED_MESSAGE,
            'group_id': group_id, 'sender_id': sender_id,
            'message': message, 'time': time
        }
        return data_message

    @staticmethod
    def build_chat_message(group_id, sender_id, message, time):
        '''
        Construct chat message for a group
        Chat messages are ones that appear in the general chat section of the app
        Params:
            group_id: The _id of the group
                Type: 'str'
            sender_id: The _id of the message sender
                Type: 'str'
            message: The message to be delivered
                Type: 'str'
            time: The time the message was sent
                Type: 'datetime.datetime'
        '''
        data_message = {
            'category': GroupMessageBuilder.MESSAGE,
            'type': GroupMessageBuilder.CHAT_MESSAGE,
            'group_id': group_id, 'sender_id': sender_id,
            'message': message, 'time': time
        }
        return data_message

    @staticmethod
    def build_system_message(group_id, sender_id, message, time):
        '''
        Construct system message for a group.
        System messages report group info to members in the group chat
        For example, removal of a member will trigger a system message
        Params:
            group_id: The _id of the group
                Type: 'str'
            sender_id: The _id of the message sender
                Type: 'str'
            message: The message to be delivered
                Type: 'str'
            time: The time the message was sent
                Type: 'datetime.datetime'
        '''
        data_message = {
            'category': GroupMessageBuilder.MESSAGE,
            'type': GroupMessageBuilder.SYSTEM_MESSAGE,
            'group_id': group_id, 'sender_id': sender_id,
            'message': message, 'time': time
        }
        return data_message


class PushService(object):
    '''
    Contains functions or sending firebase cloud messages
    '''
    def __init__(self, api_key):
        self.fcm_client = FCMNotification(api_key=api_key)

    def notify_devices(self, data, device_ids):
        '''
        Send a message to multiple devices via firebase cloud messaging.
        Params:
            data: The message to be sent.
                Type: 'dict'
            device_ids: List of the ids of the devices to send the message to.
                Type: 'list'
        '''
        self.fcm_client.notify_multiple_devices(
            registration_ids=device_ids,
            message_body=None, data_message=data
        )
