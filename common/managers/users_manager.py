'''
Operations related to user accounts
'''
from bson.objectid import ObjectId
from bson.errors import InvalidId
# from app import mongo_db
from pymongo.errors import PyMongoError
from pymongo import ASCENDING
from logging import error as log_error
from common.exceptions import EntryNotFoundError



class UsersManager(object):
    '''
    Contains functions for executing various operations on a user account
    '''

    def __init__(self, mongo_db, push_service):
        self.mongo_db = mongo_db
        self.push_service = push_service

    def _hash_uid(self, uid):
        '''
        Returns hash of then uid
        '''
        # TODO Is it really a security mesaure to keep the uids in hashed form?
        # TODO What is the best hashing algorithm to use?
        return uid

    def create_new_user(self, user_data):
        '''
        Save a new user in the database.
        Params:
            user_data: The data of the new user
                Type: 'dict'
        Returns: The id of the new user
            Type: 'bson.objectid.ObjectId'
        '''
        # set default values for fields
        user_data.update({
            "is_active": True, "read_messages": [],
            "groups": [], "devices": [],
            'photo_url': user_data.get('photo_url', None)
        })
        try:
            users_collection = self.mongo_db['users']
            result = users_collection.insert_one(document=user_data)
        except PyMongoError as err:
            log_error('UsersManager >> new_user() : ' + err.message)
            raise err
        return result.inserted_id

    def get_single_user(self, user_id, projection=None):
        '''
        Get the data for a single user
        Params:
            user_id: The _id of the user to return
                Type: str
            projection: List of the data fields to return.
                        The field names are strings.
                Type: list
        Return: The data of the user
            Type: 'dict'
        Throws:
            EntryNotFoundError: If no user exists in the database with such an id
        '''
        fields_to_select = ['email', 'name', 'photo_url',
                            'groups'] if not projection else projection
        try:
            users_collection = self.mongo_db['users']
            data = users_collection.find_one(
                filter={'_id': ObjectId(user_id), 'is_active': True},
                projection=fields_to_select
            )
        except InvalidId:
            raise EntryNotFoundError('users', user_id)
        except PyMongoError as err:
            log_error(
                "UsersManager >> get_single_user() : " + err.message)
            raise err
        if not data:
            raise EntryNotFoundError('users', user_id)
        return data

    def get_multiple_users(self, query=None, projection=None):
        '''
        Get the data for multiple users.
        If 'query' is provided,  only those that match will be fetched.
        Params:
            query: A dicionary containing mongodb query (filter) key-value pairs
                Type: 'dict'
            projection: The data fields to return.
                        Defaults to ['email', 'name', 'photo_url']
                Type: 'list'
        Return: The data for the users that matched
            Type: 'dict'
        '''
        query = {} if not query else query
        query.update({'is_active': True})
        if projection is None:
            fields_to_select = ['email', 'name', 'photo_url']
        else:
            fields_to_select = projection
        try:
            users_collection = self.mongo_db['users']
            cur = users_collection.find(
                filter=query,
                # fields that will be omitted from the result set
                projection=fields_to_select,
                sort=[('name', ASCENDING)]
            )
        except PyMongoError as err:
            log_error(
                'UsersManager >> get_multiple_users() : ' + err.message)
            raise err
        return [item for item in cur]

    def update_user_info(self, user, user_info_updates):
        '''
        Update the data for a user
        Params:
            user: The user to be updated
                Type: 'common.models.AuthUser'
            user_info_updates: Dictionary containing the new data(the update(s))
                Type: 'dict'
        '''
        try:
            old_info = self.mongo_db['users'].find_one_and_update(
                filter={'_id': user.id, 'is_active': True},
                update={'$set': user_info_updates},
                projection=['name', 'photo_url']
            )
            # TODO Move this posibly-long-running task to a Cron Service
            if old_info:  # if a document was actually modified
                # if user account's name or photo has been changed
                if 'name' in user_info_updates or 'photo_url' in user_info_updates:
                    # we have to update all nodes in groups where user's name
                    # appears
                    if 'name' in user_info_updates:
                        new_name = user_info_updates['name']
                    else:
                        new_name = old_info['name']
                    if 'photo_url' in user_info_updates:
                        new_photo_url = user_info_updates['photo_url']
                    else:
                        new_photo_url = old_info['']
                    new_user_display_info = {
                        '_id': user.id, 'name': new_name, 'photo_url': new_photo_url
                    }
                    groups_collection = self.mongo_db['groups']
                    user_groups = groups_collection.find(
                        filter={'members_ids': {
                            '$elemMatch': {'$eq': user.id}}}
                    )
                    for group in user_groups:
                        if group['creator']['_id'] == user.id:
                            group.update({'creator': new_user_display_info})
                        for item in group['admins']:
                            if item['_id'] == user.id:
                                item.update(new_user_display_info)
                        for item in group['members']:
                            if item['_id'] == user.id:
                                item.update(new_user_display_info)
                        for msg in group['messages']:
                            if msg['sender']['_id'] == user.id:
                                msg.update({'sender': new_user_display_info})
                        groups_collection.find_one_and_replace(
                            filter={'_id': group['_id']},
                            replacement=group,
                            projection=[]
                        )
            else:
                raise EntryNotFoundError('users', user.id)
        except PyMongoError as err:
            log_error('UsersManager >> update_user_info() : ' + err.message)
            raise err

    def delete_user(self, user):
        '''
        Delete a user's account. Behind the scenes, the account will be flagged as in_active
        Params:
            user: The user to be deleted
                Type: 'common.models.AuthUser'
        Throws:
            EntryNotFoundError: If the user is not found in the database
        '''
        users_collection = self.mongo_db['users']
        try:
            deleted = users_collection.find_one_and_update(
                filter={'_id': user.id},
                projection=[],
                update={'$set': {'is_active': False}}
            )
        except PyMongoError as err:
            log_error(
                'UsersManager >> delete_user() : ' + err.message)
            raise err
        if not deleted:
            raise EntryNotFoundError('users', user.id)

    # def request_to_join_group(self, user_id, group_id):
    #     '''
    #     Add the user account whose _id is as specified in 'user_id' to the
    #     subscription request list of the group whose _id is as specified in 'group_id'
    #     '''
    #     try:
    #         groups_collection = self.mongo_db['groups']
    #         result = groups_collection.update_one(
    #             {'_id': ObjectId(group_id)},
    #             {'$addToSet': {'join_requests': ObjectId(user_id)}}
    #         )
    #     except InvalidId:
    #         raise EntryNotFoundError('groups', group_id)
    #     except PyMongoError as err:
    #         log_error('UsersManager >> request_to_join_group() : ' + err.message)
    #         raise err
    #     if result.modified_count == 0:
    #         raise EntryNotFoundError('groups', group_id)

    def get_user_groups(self, user_id):
        '''
        Get the data for the groups of a user
        Params:
            user_id: The _id of the user
                Type: 'str'
        Return: A list of dicts containing the user's groups data
            Type: 'list'
        Throws:
            EntryNotFoundError: If the user is not found in the database
        '''
        user = self.get_single_user(user_id, projection=['groups'])
        from common.managers.groups_manager import GroupsManager
        groups_manager = GroupsManager(self.mongo_db, self.push_service)
        try:
            user_groups = groups_manager.get_multiple_groups(
                query={'_id': {'$in': user['groups']}},
                projection=['name', 'photo_url', 'date_created', 'admins', 'creator']
            )
        except PyMongoError as err:
            log_error(__name__ + ' >> UsersManager >> get_user_groups() : ' + err.message)
            raise err
        return user_groups

    def leave_group(self, user, group_id):
        '''
        Remove user from group
        Params:
            user_id: The user leaving the group
                Type: 'common.models.AuthUser'
            group_id: The _id of the group
                Type: 'str'
        Throws:
            EntryNotFoundError: If the user or group is not found
        '''
        from common.managers.groups_manager import GroupsManager
        group_manager = GroupsManager(self.mongo_db, self.push_service)
        group_manager.delete_group_member(user, group_id, str(user.id))


    def notify_user(self, user_id, message):
        '''
        Send message to user.
        Params:
            user_id: The _id of the user to send the message to
                Type: str
            message: The message data to send
                Type: dict
        Throws:
            EntryNotFoundError: If the user is not found
        '''
        user_devices = self.get_single_user(
            user_id=user_id, projection=['devices']
        )['devices']
        self.push_service.notify_devices()
    
    def add_user_device(self, user_id, device_id):
        '''
        Add a device to the list of devices of a user
        Params:
            user_id: The id of the user
            device_id: The id of the device
        Throws:
            EntryNotFoundError: if the user was not found in the database
            PyMongoError: If there is an error while updating the user's info
        '''
        users_collection = self.mongo_db['users']
        try:
            added = users_collection.find_one_and_update(
                filter={'_id': ObjectId(user_id)},
                projection=[],
                update={'$pull': {'devices': device_id}}
            )
        except InvalidId:
            raise EntryNotFoundError('users', user_id)
        except PyMongoError as err:
            log_error(
                'UsersManager >> add_user_device() : ' + err.message)
            raise err
        if not added:
            raise EntryNotFoundError('users', user_id)

    def delete_user_device(self, user_id, device_id):
        '''
        Remove a user from the list of devices of a user
        Params:
            user_id: The id of the user
            device_id: The id of the device
        Throws:
            EntryNotFoundError: if the user was not found in the database
            PyMongoError: If there is an error while updating the user's info
        '''
        users_collection = self.mongo_db['users']
        try:
            added = users_collection.find_one_and_update(
                filter={'_id': ObjectId(user_id)},
                projection=[],
                update={'$addToSet': {'devices': device_id}}
            )
        except InvalidId:
            raise EntryNotFoundError('users', user_id)
        except PyMongoError as err:
            log_error(
                'UsersManager >> delete_user_device() : ' + err.message)
            raise err
        if not added:
            raise EntryNotFoundError('users', user_id)

    def update_user_device(self, user_id, old_device_id, new_device_id):
        '''
        Remove a user from the list of devices of a user.

        Params:
            user_id: The id of the user
            old_device_id: The id of the device to be replaced/updated
            new_device_id: The id of the new device to be inserted

        Throws:
            EntryNotFoundError: if the user was not found in the database
            PyMongoError: If there is an error while updating the user's info
        '''
        users_collection = self.mongo_db['users']
        try:
            added = users_collection.find_one_and_update(
                filter={'_id': ObjectId(user_id), 'devices': old_device_id},
                projection=[],
                update={'$update': {'devices.$': new_device_id}}
            )
        except InvalidId:
            raise EntryNotFoundError('users', user_id)
        except PyMongoError as err:
            log_error(
                'UsersManager >> update_user_device() : ' + err.message)
            raise err
        if not added:
            raise EntryNotFoundError('users', user_id)
