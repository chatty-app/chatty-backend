'''
User Authentication related operations
'''
from flask import abort
import urllib
import json
from jose import jwt
from jose.exceptions import JWTError
from logging import error as log_error
from common.status_codes import STATUS_UNAUTHORIZED
from os import environ as os_environ
from flask import g, abort
from common.status_codes import STATUS_UNAUTHORIZED
from functools import wraps


def get_firebase_uid_from_token(token):
    '''
    Get the decoded uid from the jwt provided.
    If decoding resulted in an error, None is returned
    '''
    token = token.replace('\r', '').replace('\n', '')
    return token
    # target_audience = os_environ.get('FIREBASE_APP_ID')
    # certificate_url = 'https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com'
    # response = urllib.urlopen(certificate_url)
    # certs = response.read()
    # certs = json.loads(certs)
    # try:
    #     user = jwt.decode(token, certs, algorithms='RS256',
    #                       audience=target_audience)
    # except JWTError as err:
    #     log_error(__name__ + ' >> get_firebase_uid_from_token() : ' + str(err.message))
    #     abort(STATUS_UNAUTHORIZED)
    # else:
    #     return user.get('uid', '...')


def auth_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        auth_user = getattr(g, 'auth_user', None)
        if auth_user is None:
            abort(STATUS_UNAUTHORIZED)
    return wrapper
