'''
HTTP Status codes for possible response/error types in the application
Those labelled X5X are custom to this application;
they are not part of the standard HTTP status codes
'''

STATUS_NO_INPUT = 450
STATUS_NOT_FOUND = 404
STATUS_INVALID_INPUT = 451
STATUS_LESSON_CLASH_ERROR = 452
STATUS_UNAUTHORIZED = 401


STATUS_DB_ERROR = 554
STATUS_FILE_UPLOAD_ERROR = 554
STATUS_INTERNAL_SERVER_ERROR = 500


STATUS_OK = 200
STATUS_CREATED = 201
