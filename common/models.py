class AuthUser(object):
    '''
    Represents the currently authenticated user
    '''
    def __init__(self, user_data):
        self.name = user_data['name']
        self.id = user_data['_id']
        self.email = user_data['email']
        self.photo_url = user_data['photo_url']
        self.display_info = {
            'name':self.name, '_id': self.id,
            'email': self.email, 'photo_url': self.photo_url
        }

# class User(object):

#     def __init__(self, **kwargs):
#         self.user_id = kwargs.get('user_id')
#         self.fullname = kwargs.get('fullname')
#         self.email = kwargs.get('email')
#         self.password = kwargs.get('password')
#         self.is_course_rep = kwargs.get('is_course_rep')
#         self.is_admin = kwargs.get('is_admin')
#         self.is_active = kwargs.get('is_active', True)


# class Venue(object):

#     def __init__(self, **kwargs):
#         self.id = kwargs.get('id')
#         self.name = kwargs.get('name')
#         self.description = kwargs.get('description')
#         self.college = kwargs.get('college')
#         self.is_active = kwargs.get('is_active', True)


# class College(object):

#     def __init__(self, **kwargs):
#         self.college_id = kwargs.get('college_id')
#         self.name = kwargs.get('name')


# class Lesson(object):

#     def __init__(self, **kwargs):
#         self.lesson_id = kwargs.get('lesson_id')
#         self.day_of_week = kwargs.get('day_of_week')
#         self.start_time = kwargs.get('start_time')
#         self.end_time = kwargs.get('end_time')
#         self.venue = kwargs.get('venue_id')
#         self.course_name = kwargs.get('course_name')
#         self.course_code = kwargs.get('course_code')
#         self.is_one_time = kwargs.get('is_one_time')
#         self.poster = kwargs.get('poster')
#         self.date_posted = kwargs.get('date_posted')

#         # miscellaenous attributes
#         if self.day_of_week is not None:
#             self.day_of_week_str = self._get_day_of_week_str()
#         self.type_of_clash = kwargs.get('type_of_clash')

#     def _get_day_of_week_str(self):
#         days = {
#             1: 'SUNDAY',
#             2: 'MONDAY',
#             3: 'TUESDAY',
#             4: 'WEDNESDAY',
#             5: 'THURSDAY',
#             6: 'FRIDAY',
#             7: 'SATURDAY'
#         }
#         try:
#             # check if the day_of_week is an integer
#             day_of_week_int = int(self.day_of_week)
#         except ValueError:
#             return str(self.day_of_week)
#         else:
#             return days.get(day_of_week_int, "None")


# class Message(object):
#     def __init__(self, **kwargs):
#         self.message_id = kwargs.get('id')
#         self.message = kwargs.get('message')
#         self.has_attachment = kwargs.get('has_attachment')
#         self.date_sent = kwargs.get('date_sent')
#         self.time_to_live = kwargs.get('time_to_live')
#         self.poster = kwargs.get('poster')
        