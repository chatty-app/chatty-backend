'''
All request associated with the group_messages endpoint
'''
from flask import request, jsonify, abort, make_response, g
from flask_restful import Resource
from common.status_codes import STATUS_UNAUTHORIZED, STATUS_OK, STATUS_NOT_FOUND
from common.exceptions import EntryNotFoundError, UnAuthorizedAccessError
from common.managers.groups_manager import GroupsManager
from common.auth_service import auth_required


class GroupMessagesResource(Resource):
    '''
    Hanldes requests associated to groups' messages
    '''
    manager = GroupsManager(getattr(g, '_mongo_db'), getattr(g, '_push_service'))
    decorator = [auth_required]

    def get(self, group_id):
        '''
        GET the messages of a group
        Params:
            group_id: The _id of the group
                Type: 'str'
        Throws:
            STATUS_NOT_FOUND: If the group is not found
            STATUS_UNAUTHORIZED: The operation is not authorized. This happens if:
                                1.  The current authed user is not a member of the group
        '''        
        pass

    def post(self, group_id):
        '''
        POST  a message to a group
        Params:
            group_id: The _id of the group
                Type: 'str'
        Throws:
            STATUS_UNAUTHORIZED: The operation is not authorized. This happens if:
                                1.  The message is a prioritized one and the current
                                    authed user is not an admin of the group
        '''        
        pass
