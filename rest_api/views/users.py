'''
Requests directed towards the 'users' endpoint
'''
from flask import jsonify, request, g, make_response, abort
from flask_restful import Resource

# from common.db_service import self.db_service
# from app import db_service as self.db_service
from common.exceptions import EntryNotFoundError
from common.status_codes import *
from common.schema import NewUserSchema, UserUpdateSchema
from common.managers.users_manager import UsersManager
from common.auth_service import get_firebase_uid_from_token, auth_required
import logging


class UsersResource(Resource):
    '''
    Handles incoming requests for the 'users' endpoint
    '''
    manager = UsersManager(getattr(g, '_mongo_db'), getattr(g, '_push_service'))    

    def hash_password(self, password):
        '''
        Return the hashed output of 'password'
        '''
        return password

    def get(self, user_id=None):
        '''
        Get the data for all users.
        Get the data for a specific user.
        '''
        if g.auth_user is None:
            abort(STATUS_UNAUTHORIZED)
        if user_id:
            try:
                user = self.manager.get_single_user(user_id)
            except EntryNotFoundError as err:
                return {'message': err.message}, STATUS_NOT_FOUND
            response = make_response(jsonify(user), STATUS_OK)
        else:
            users = self.manager.get_multiple_users()
            response = make_response(jsonify(users), STATUS_OK)
        return response

    def post(self):
        '''
        Create a new user. User's data must be passed in the POST data
        '''
        user_data, errors = NewUserSchema().load(request.form.to_dict())
        if errors:
            return {'errors': errors}, STATUS_INVALID_INPUT
        firebase_token = user_data.pop('firebase_token')
        user_uid = get_firebase_uid_from_token(firebase_token)
        user_data.update({'uid':user_uid})
        user_id = self.manager.create_new_user(user_data)
        return make_response(jsonify({'id': user_id}), STATUS_CREATED)

    def put(self):
        '''
        UPDATE the data for the user whose id is specified
        '''
        if g.auth_user is None:
            abort(STATUS_UNAUTHORIZED)
        schema = UserUpdateSchema()
        updates, errors = schema.load(request.form.to_dict())
        if errors:
            return {'errors': errors}, STATUS_INVALID_INPUT
        try:
            self.manager.update_user_info(g.auth_user, updates)
        except EntryNotFoundError as err:
            return {'message': err.message}, STATUS_NOT_FOUND
        message = 'Updated successfully'
        return make_response(jsonify({'message': message}), STATUS_OK)

    def delete(self):
        '''
        Delete user from the system.
        Technically, user will just be flagged as inactive in the database
        '''
        if g.auth_user is None:
            abort(STATUS_UNAUTHORIZED)
        try:
            self.manager.delete_user(g.auth_user)
        except EntryNotFoundError as err:
            return {'message': err.message}, STATUS_NOT_FOUND
        message = 'Deleted successfully'
        return {'message': message}, STATUS_OK
