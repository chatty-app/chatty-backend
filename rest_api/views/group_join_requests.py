'''
Request to join a group.
'''
from flask import g, make_response, jsonify, abort
from flask_restful import Resource

from common.status_codes import *
from common.exceptions import UnAuthorizedAccessError, EntryNotFoundError
from common.managers.groups_manager import GroupsManager
from common.auth_service import auth_required


class GroupJoinRequestsResource(Resource):
    '''
    User requests to join a group
    '''
    manager = GroupsManager(getattr(g, '_mongo_db'), getattr(g, '_push_service'))
    decorators = [auth_required]

    def get(self, group_id):
        '''
        GET the users who have requested to join a group
        Params:
            group_id: The _id of the group
                Type: str
        Throws:
            STATUS_NOT_FOUND: If group was not found
            STATUS_UNAUTHORIZED: If the auth_user is not authorized to view this info
        '''
        try:
            requests = self.manager.get_requests_to_join_group(g.auth_user, group_id)
        except EntryNotFoundError as not_found_err:
            return {'message': not_found_err.message}, STATUS_NOT_FOUND
        except UnAuthorizedAccessError as unauth_err:
            return {'message': unauth_err.message}, STATUS_UNAUTHORIZED
        return make_response(jsonify(requests), STATUS_OK)

    def post(self, group_id):
        '''
        Make a request to join a group.
        The _id of the requestor must be passed in request.form
        Params:
            group_id: The _id of the group
                Type: str
        Throws:
            STATUS_NOT_FOUND: If the group or user is not found
            STATUS_UNAUTHORIZED: If the access is not authorized. This can occur if:
                                1.  The currently authenticated user is None
                                2.  The user is already a member of the group
        '''
        try:
            self.manager.make_request_to_join_group(group_id, g.auth_user)
        except EntryNotFoundError as not_found_err:
            return {'message': not_found_err.message}, STATUS_NOT_FOUND
        except UnAuthorizedAccessError as unauth_err:
            return {'message': unauth_err.message}, STATUS_UNAUTHORIZED
        return {'message': 'Successfully placed request'}, STATUS_OK

    def put(self, group_id, user_id):
        '''
        Confirm a user's request to join a group.
        Params:
            group_id: The _id of the group
                Type: 'str'
            user_id: The _id of the user to confirm
                Type: 'str'
        Throws:
            STATUS_NOT_FOUND: If the  group or user was not found
            STATUS_UNAUTHORIZED: If the operation is not authorized. This happens if:
                                1.  The currently authenticated user is not an admin of the group
                                2.  The user has not made a request to join the group
        '''
        try:
            self.manager.confirm_request_to_join_group(g.auth_user, group_id, user_id)
        except EntryNotFoundError as not_found_err:
            return {'message': not_found_err.message}, STATUS_NOT_FOUND
        except UnAuthorizedAccessError as unauth_err:
            return {'message': unauth_err.message}, STATUS_UNAUTHORIZED


    def delete(self, group_id, user_id):
        '''
        Cancel user's request to join a group
        Params:
            group_id: The _id of the group
                Type: 'str'
            user_id: The _id of the user
                Type: 'str'
        Throws:
            STATUS_NOT_FOUND: If the group was not found
            STATUS_UNAUTHORIZED: If the access is not authorized. This can occur if:
                                1. The currently authenticated user is None
        '''
        try:
            self.manager.cancel_request_to_join_group(g.auth_user, group_id, user_id)
        except EntryNotFoundError as not_found_err:
            return {'message': not_found_err.message}, STATUS_NOT_FOUND
        return {'message': 'Request has been cancelled successfully'}, STATUS_OK
